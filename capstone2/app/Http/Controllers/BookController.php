<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Language;
use \App\Condition;
use \App\Book;
use Session;
use Auth;

class BookController extends Controller
{
    public function books(){
    	$books = Book::all();
    	$categories = Category::all();
    	$languages = Language::all();
    	$conditions = Condition::all();

        return view('bookshelves', compact('books', 'categories', 'languages', 'conditions'));
    }

    public function produce(Request $req){
    	$categories = Category::all();
    	$languages = Language::all();
    	$conditions = Condition::all();

    	return view('adminviews.addbook', compact('categories', 'languages', 'conditions'));
    }

    public function keep(Request $req){
    	// validate
    	$needs = array(
    		"title" => "required",
    		"author" => "required",
    		"publisher" => "required",
    		"category_id" => "required",
    		"language_id" => "required",
    		"condition_id" => "required",
    		"imgPath" => "required|image|mimes:jpeg, jpg, png, gif, tiff, tif, webp, bitmap|max:2048"
    	);

    	$this->validate($req, $needs);
    	// dd($req);

    	// capture
    	$newBook = new Book;
    	$newBook->title = $req->title;
    	$newBook->author_id = $req->author;
    	$newBook->publisher_id = $req->publisher;
    	$newBook->category_id = $req->category_id; 
    	$newBook->language_id = $req->language_id;
    	$newBook->condition_id = $req->condition_id;

    	// image handling
    	$image = $req->file('imgPath');
    	//rename the image
    	$image_name = time().".".$image->getClientOriginalExtension();

    	$target = "images/"; //corresponds to the public images directory

    	$image->move($target, $image_name);

    	$newBook->imgPath = $target.$image_name;

  //   	// save
    	$newBook->save();

    	Session::flash("message", "$newBook->name has been placed to shelf");

		// redirect
    	return redirect('/bookshelves');
    }

    public function editBook(Request $req){
    	$categories = Category::all();
    	$languages = Language::all();
    	$conditions = Condition::all();

    	return view('adminviews.editbook', compact('categories', 'languages', 'conditions', 'books'));
    }
}
