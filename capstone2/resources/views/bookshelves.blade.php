@extends('layouts.app')
@section('content')

    <h1 class="text-center text-3xl font-sans font-bold py-5">Books</h1>
    
    @if(Session::has("message"))
        <h5 class="text-center my-5 py-2 ml-10" style="color: yellowgreen; background-color: rgba(255,255,255, 50%);">{{Session::het('message')}}</h5>
    @endif

    <div class="container h-screen flex mt-16">
        {{-- @foreach($books as $indiv_book) --}}
            <div class="w-3/12 shadow-lg bg-gray-200 border py-2 rounded-lg text-center ml-10" style="height: 73%">
                <div class="flex justify-center mt-3">
                    <img src="/images/education.jpg" alt="" class="h-48 w-auto">
                </div>
                
                <div class="items-baseline">
                    <h1 class="text-xl mt-3">All About Education</h1>
                    <br>
                    <p class="text-sm mx-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur assumenda, animi voluptate cumque aliquam eum minus iure quae illum ab dolorum corrupti, corporis nulla reiciendis, quidem quam! Dolor, magni quasi?</p>
                    <br>
                    <span class="text-sm">#
                    
                    </span>
                    <span class="ml-2 text-sm">#</span>
                    <span class="ml-2 text-sm">#</span>
                </div>
                <div class="flex justify-center mt-3">
                    <form action="" method="POST">
                        <button class="bg-green-700 py-5 px-4 rounded-l-lg text-lg font-bold text-white hover:bg-green-500 hover:text-blue-900" type="submit">BORROW</button> 
                    </form>
                    <form action="" method="POST">
                        @csrf
                        @method('PATCH')
                        <a href="" class="bg-yellow-600 py-5 px-5 text-lg font-bold text-white hover:bg-yellow-400 hover:text-blue-900" type="submit">EDIT</a>
                    </form>
                    <form action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="" class="bg-red-700 py-5 px-4 rounded-r-lg text-lg font-bold text-white hover:bg-red-500 hover:text-blue-900" type="submit">DELETE</a>
                    </form>
                </div>    
            </div>
        {{-- @endforeach     --}}
    </div>

@endsection