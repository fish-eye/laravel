<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Bookshelves
Route::get('/bookshelves', 'BookController@books');

// Admin
// Add book
Route::get('/addbook', 'BookController@produce');
Route::post('/addbook', 'BookController@keep');

// Edit book
Route::get('/editbook', 'BookController@editBook');