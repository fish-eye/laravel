<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Solution;

class SolutionController extends Controller
{
    public function destroy($id){
		$solToDelete = Solution::find($id);
    	$solToDelete->delete();
    	return redirect()->back();
	}    
}
