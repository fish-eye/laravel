@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Add Item</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/additem" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="name">Item Name:</label>
			<input type="text" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="price">Price:</label>
			<input type="number" name="price" class="form-control">
		</div>
		<div class="form-group">
			<label for="description">Description:</label>
			<textarea name="description" class="form-control"></textarea>
		</div>
		<div class="form-group">
			<label for="imgPath">Image:</label>
			<input type="file" name="imgPath" class="form-control">
		</div>
		<div class="form-group">
			<label for="category_id">Category:</label>
			<select type="select" name="category_id" class="form-control">
				@foreach($categories as $indiv_category)
					<option class="form-control" value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="text-center">
			<button type="submit" class="btn btn-success">Add Item</button>
		</div>
	</form>
</div>

@endsection