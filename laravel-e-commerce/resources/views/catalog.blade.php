@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Featured Gadgets</h1>
<div class="d-flex justify-content-end">
	<div class="col-lg-5">
		<form class="p-3" action="/search" method="POST">
			@csrf
			<div class="input-group">
				<input type="" name="" class="from-control" placeholder="Search items by name, category">
				<div class="input-group-append">
					<button class="btn btn-info" type="submit">Search</button>
				</div>
			</div>
		</form>
	</div>
</div>

@if(Session::has("message"))
	<h5 class="text-center my-5 py-2" style="color: yellowgreen; background-color: rgba(255,255,255, 50%);">{{Session::get('message')}}</h5>
@endif

<div class="container">
	<div class="row">
		<div class="col-lg-2">
			<h4>Filter by Category</h4>

			<ul class="list-group">
				@foreach($categories as $category)
					<li class="list-group-item">
						<a href="/catalog/{{$category->id}}">{{$category->name}}</a>
					</li>
				@endforeach
				<li class="list-group-item">
					<a href="/catalog">All</a>
				</li>
			</ul>
			<hr>

			<h4>Sort by Price</h4>
			<ul class="list-group">
				<li class="list-group-item">
					<a href="/catalog/sort/asc">Cheapest First</a>
				</li>
				<li class="list-group-item">
					<a href="/catalog/sort/desc">Most Expensive First</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-10">
			<div class="row">
				@foreach($items as $indiv_item)
					<div class="col-lg-4 p-3 my-2">
						<div class="card">
							<img src="{{asset($indiv_item->imgPath)}}" height="300px" alt="item image" class="card-img-top">
							<div class="card-body">
								<h3 class="card-title">{{$indiv_item->name}}</h3>
								<p class="card-text">Price: ${{$indiv_item->price}}</p>
								<p class="card-text">Description: {{$indiv_item->description}}</p>
								<p class="card-text">Category: {{$indiv_item->category->name}}</p>
							</div>
							<div class="card-footer text-center d-flex justify-content-center m-3">
								<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger" type="submit">Delete</button>
								</form>
								<form action="/edititem/{{$indiv_item->id}}" method="POST">
									@csrf
									@method('PATCH')
									<a href="/edititem/{{$indiv_item->id}}" class="btn btn-info">Edit</a>
								</form>
							</div>
							<div class="card-footer text-center d-flex justify-content-center flex-direction-column">
								{{-- <form action="/addtocart/{{$indiv_item->id}}" method="POST">
									@csrf --}}
									<input type="number" name="quantity" class="from-control text-center" value="1" id="quantity_{{$indiv_item->id}}">
									<button class="btn btn-primary" onclick="addToCart({{$indiv_item->id}})" type="submit">Add to Cart</button>
								{{-- </form> --}}
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		const addToCart = id =>{
			
			let quantity = document.querySelector("#quantity_"+id).value;

			// alert(quantity + " of item " + itemName + " has been added to cart");

			let data = new FormData;

			data.append("_token", "{{ csrf_token() }}");
			data.append("quantity", quantity);

			fetch("/addtocart/"+id, {
				method: "POST",
				body: data
			}).then(res=>res.text())
			.then(res=>console.log(res))
		}
	</script>
</div>
@endsection