<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Catalog
Route::get('/catalog', 'ItemController@itemCards');
Route::get('/catalog/{id}', 'ItemController@filter');
Route::get('/catalog/sort/{sort}', 'ItemController@sort');
Route::post('/search', 'ItemController@search');

// Admin
Route::middleware("admin")->group(function(){
	Route::get ('/allorders', 'OrderController@index');
	Route::get('/additem', 'ItemController@create');
	Route::post('/additem', 'ItemController@store');
	Route::delete('/deleteitem/{id}', 'ItemController@destroy');
	Route::patch('/edititem/{id}', 'ItemController@update');
	Route::get('/edititem/{id}', 'ItemController@edit');
	Route::get ('/allusers', 'UserController@index');
	Route::get('/changerole/{id}', 'UserController@changRole');
	Route::delete ('/deleteuser/{id}', 'UserController@deleteUser');
});

// Users
Route::middleware('user')->group(function(){
	// Cart CRUD
	Route::post('/addtocart/{id}', 'ItemController@addToCart');
	Route::get('/cart', 'ItemController@showCart');
	Route::delete('/removeitem/{id}', 'ItemController@removeItem');
	Route::get('emptycart', 'ItemController@emptyCart');
	Route::get('checkout', 'OrderController@checkOut');
	Route::get ('/showorders', 'OrderController@showOrders');
});

Route::middleware('auth')->group(function(){
	Route::get ('/cancelorder/{id}', 'OrderController@cancelOrderByAdmin');
});
