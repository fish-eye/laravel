<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Category;
use \App\Status;

class Todo extends Model
{
    public function category() {
    	return $this->belongsTo("\App\Category");
    }

    public function status() {
    	return $this->belongsTo("\App\Status");
    }
}
