@extends('layouts.app')
@section('content')
<h1 class="text-center py-5">Add Task</h1>
<div class="col-lg-4 offset-lg-4">
	<form action="/addtask" method="POST">
		@csrf
		<input type="text" name="title" class="form-control" placeholder="Enter task title">
		<input type="text" name="body" class="form-control" placeholder="Enter task body">
		<div class="from-group">
			<label for="category_id">Category</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $indiv_category)
				<option value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="text-center">
			<button class="btn btn-info" type="submit">Add Task</button>
		</div>
	</form>
</div>
@endsection