@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">TODO LIST</h1>

<div class="row">
	@foreach($tasks as $indiv_task)
		<div class="col-lg-3 my-2">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title text-center">{{$indiv_task->title}}</h4>
					<p class="card-text">{{$indiv_task->body}}</p>
					<p class="card-text">{{$indiv_task->category->name}}</p>
					<p class="card-text">{{$indiv_task->status->name}}</p>
				</div>
				<div class="card-footer text-center">
					<form action="/deletetask/{{$indiv_task->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit">Delete Task</button>
					</form>
					<form action="/markasdone/{{$indiv_task->id}}" method="POST">
						@csrf
						@method('PATCH')
						<button class="btn btn-success">@if($indiv_task->status_id != 1)<span>Mark as Pending</span>
							@else
							<span>Mark as Done</span>
						@endif</button>
					</form>
				</div>
			</div>
		</div>
	@endforeach
</div>



@endsection