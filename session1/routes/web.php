<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/blade1', function () {
	return view('blade1');
});

Route::get('/blade2', function () {
	return view('blade2');
});

Route::get('/blade3', function () {
	return view('blade3');
});

Route::get('/welcome', 'Controller@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// to show all tasks
Route::get('/tasks', 'TodoController@index');

// to show add task form
Route::get('/addtask', 'TodoController@create');

// to save the new Task
Route::post('/addtask', 'TodoController@store');

// to delete task
Route::delete('/deletetask/{id}', 'TodoController@destroy');

// to mark as done
Route::patch('/markasdone/{id}', 'TodoController@markAsDone');